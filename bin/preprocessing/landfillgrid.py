#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 28 17:28:25 2021

@author: janiswissinger
"""

import subprocess
import numpy as np
import fileinput
import math


pIdx = 1
lIdx = 1

filenameOutput = "landfillgrid"

filenameGeo  = filenameOutput+'.geo'
geoFile = open(filenameGeo, 'w')

#length_area_after_translation = ...

l_a = 4500.0 # length_area_before_translation # length_area_before_translation = sqrt(height^2 - length_area_after_translation^2)
   
w_a = 900.0 #width_area
   
l_l = 300.0 # length_landfill

w_l = 300.0 # width_landfill

l_t_l = 300.0 # length_till_landfill

w_t_l = 300.0 # width_till_landfill

l_b_l = l_a - l_t_l - l_l # length_behind_landfill

w_b_l = w_a - w_t_l - w_l # width_behind_landfill

h_a = 27.5 # height_area

h_l = 20.0 # height_landfill

h_a_l = h_a + h_l #height_area_landfill
   

grid_l_a = 150#50.0

grid_w_a = 150#50.0

grid_l_l = 30#10.0

grid_w_l = 30#10.0

p_l_a = 1.2 # progression_length_area


#grid_l_t_l = 10.0

#grid_w_t_l = 10.0

#grid_l_b_l = 50.0

#grid_w_b_l = 10.0



height = 530.0

delta_height = height/l_a

angle_translation = math.atan(l_a/height)

z_translation_area = math.sin(angle_translation) * h_a
z_translation_landfill = math.sin(angle_translation) * h_l
z_translation_area_landfill = math.sin(angle_translation) * h_a_l
 
#x_translation_area = math.cos(angle_translation) * h_a
 
cot_angle_translation = 1/math.tan(angle_translation)
 

length_finer_grid = 700.0
length_wider_grid = 3300.0

#delta_z_translation = z_translation/l_a
#delta_x_translation = x_translation/z_translation

geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(1, 0.0, 0.0, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(2, 0.0, w_t_l, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(3, 0.0, w_t_l + w_l, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(4, 0.0, w_a, 0.0, 100.0 ))

geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(5, l_t_l, 0.0, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(6, l_t_l, w_t_l, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(7, l_t_l, w_t_l + w_l, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(8, l_t_l, w_a, 0.0, 100.0 ))

geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(9, l_t_l + l_l, 0.0, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(10, l_t_l + l_l, w_t_l, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(11, l_t_l + l_l, w_t_l + w_l, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(12, l_t_l + l_l, w_a, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(13, l_a, 0.0, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(14, l_a, w_t_l, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(15, l_a, w_t_l + w_l, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(16, l_a, w_a, 0.0, 100.0 ))


geoFile.write('Line(%s) = {%s, %s};\n' %(1, 1, 2))
geoFile.write('Transfinite Line{%s} = %s;\n' %(1, w_t_l/grid_w_a +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(2, 3, 2))
geoFile.write('Transfinite Line{%s} = %s;\n' %(2, w_l/grid_w_l +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(3, 3, 4))
geoFile.write('Transfinite Line{%s} = %s;\n' %(3, w_b_l/grid_w_a +1 ))

geoFile.write('Line(%s) = {%s, %s};\n' %(4, 5, 1))
geoFile.write('Transfinite Line{%s} = %s;\n' %(4, l_t_l/grid_l_a +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(5, 2, 6))
geoFile.write('Transfinite Line{%s} = %s;\n' %(5, l_t_l/grid_l_a +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(6, 7, 3))
geoFile.write('Transfinite Line{%s} = %s;\n' %(6, l_t_l/grid_l_a +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(7, 4, 8))
geoFile.write('Transfinite Line{%s} = %s;\n' %(7, l_t_l/grid_l_a +1 ))

geoFile.write('Line(%s) = {%s, %s};\n' %(8, 6, 5))
geoFile.write('Transfinite Line{%s} = %s;\n' %(8, w_t_l/grid_w_a +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(9, 6, 7))
geoFile.write('Transfinite Line{%s} = %s;\n' %(9, w_l/grid_w_l +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(10, 8, 7))
geoFile.write('Transfinite Line{%s} = %s;\n' %(10, w_b_l/grid_w_a +1 ))

geoFile.write('Line(%s) = {%s, %s};\n' %(11, 5, 9))
geoFile.write('Transfinite Line{%s} = %s;\n' %(11, l_l/grid_l_l +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(12, 10, 6))
geoFile.write('Transfinite Line{%s} = %s;\n' %(12, l_l/grid_l_l +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(13, 7, 11))
geoFile.write('Transfinite Line{%s} = %s;\n' %(13, l_l/grid_l_l +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(14, 12, 8))
geoFile.write('Transfinite Line{%s} = %s;\n' %(14, l_l/grid_l_l +1 ))

geoFile.write('Line(%s) = {%s, %s};\n' %(15, 9, 10))
geoFile.write('Transfinite Line{%s} = %s;\n' %(15, w_t_l/grid_w_a +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(16, 11, 10))
geoFile.write('Transfinite Line{%s} = %s;\n' %(16, w_l/grid_w_l +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(17, 11, 12))
geoFile.write('Transfinite Line{%s} = %s;\n' %(17, w_b_l/grid_w_a +1 ))

geoFile.write('Line(%s) = {%s, %s};\n' %(18, 13, 9))
geoFile.write('Transfinite Line{%s} = %s;\n' %(18, l_b_l/grid_l_a +1))
geoFile.write('Line(%s) = {%s, %s};\n' %(19, 10, 14))
geoFile.write('Transfinite Line{%s} = %s;\n' %(19,l_b_l/grid_l_a +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(20, 15, 11))
geoFile.write('Transfinite Line{%s} = %s;\n' %(20, l_b_l/grid_l_a +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(21, 12, 16))
geoFile.write('Transfinite Line{%s} = %s;\n' %(21, l_b_l/grid_l_a +1 ))
'''
geoFile.write('Line(%s) = {%s, %s};\n' %(18, 13, 9))
geoFile.write('Transfinite Line{%s} = %s Using Progression %s;\n' %(-18, 30.0 +1, p_l_a ))
geoFile.write('Line(%s) = {%s, %s};\n' %(19, 10, 14))
geoFile.write('Transfinite Line{%s} = %s Using Progression %s;\n' %(19, 30.0 +1, p_l_a ))
geoFile.write('Line(%s) = {%s, %s};\n' %(20, 15, 11))
geoFile.write('Transfinite Line{%s} = %s Using Progression %s;\n' %(-20, 30.0 +1, p_l_a ))
geoFile.write('Line(%s) = {%s, %s};\n' %(21, 12, 16))
geoFile.write('Transfinite Line{%s} = %s Using Progression %s;\n' %(21, 30.0 +1, p_l_a ))
'''
geoFile.write('Line(%s) = {%s, %s};\n' %(22, 14, 13))
geoFile.write('Transfinite Line{%s} = %s;\n' %(22, w_t_l/grid_w_a +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(23, 14, 15))
geoFile.write('Transfinite Line{%s} = %s;\n' %(23, w_l/grid_w_l +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(24, 16, 15))
geoFile.write('Transfinite Line{%s} = %s;\n' %(24, w_b_l/grid_w_a +1 ))
geoFile.write('\n')

geoFile.write('Line Loop(%s) = {%s, %s, %s, %s};\n' %(1, 1, 5, 8, 4))
#Surface
geoFile.write('Plane Surface(1) = {1};\n')
geoFile.write('Transfinite Surface{1} = {1, 2, 6, 5};\n')
geoFile.write('Recombine Surface{1};\n')
geoFile.write('\n')

geoFile.write('Line Loop(%s) = {%s, %s, %s, %s};\n' %(2, 2, 5, 9, 6))
#Surface
geoFile.write('Plane Surface(2) = {2};\n')
geoFile.write('Transfinite Surface{2} = {3, 2, 6, 7};\n')
geoFile.write('Recombine Surface{2};\n')
geoFile.write('\n')

+geoFile.write('Line Loop(%s) = {%s, %s, %s, %s};\n' %(3, 3, 7, 10, 6))
#Surface
geoFile.write('Plane Surface(3) = {3};\n')
geoFile.write('Transfinite Surface{3} = {3, 4, 8, 7};\n')
geoFile.write('Recombine Surface{3};\n')
geoFile.write('\n')

geoFile.write('Line Loop(%s) = {%s, %s, %s, %s};\n' %(4, 8, 11, 15, 12))
#Surface
geoFile.write('Plane Surface(4) = {4};\n')
geoFile.write('Transfinite Surface{4} = {6, 5, 9, 10};\n')
geoFile.write('Recombine Surface{4};\n')
geoFile.write('\n')

geoFile.write('Line Loop(%s) = {%s, %s, %s, %s};\n' %(5, 9, 13, 16, 12))
#Surface
geoFile.write('Plane Surface(5) = {5};\n')
geoFile.write('Transfinite Surface{5} = {6, 7, 11, 10};\n')
geoFile.write('Recombine Surface{5};\n')
geoFile.write('\n')

geoFile.write('Line Loop(%s) = {%s, %s, %s, %s};\n' %(6, 10, 13, 17, 14))
#Surface
geoFile.write('Plane Surface(6) = {6};\n')
geoFile.write('Transfinite Surface{6} = {8, 7, 11, 12};\n')
geoFile.write('Recombine Surface{6};\n')
geoFile.write('\n')

geoFile.write('Line Loop(%s) = {%s, %s, %s, %s};\n' %(7, 15, 19, 22, 18))
#Surface
geoFile.write('Plane Surface(7) = {7};\n')
geoFile.write('Transfinite Surface{7} = {9, 10, 14, 13};\n')
geoFile.write('Recombine Surface{7};\n')
geoFile.write('\n')

geoFile.write('Line Loop(%s) = {%s, %s, %s, %s};\n' %(8, 16, 19, 23, 20))
#Surface
geoFile.write('Plane Surface(8) = {8};\n')
geoFile.write('Transfinite Surface{8} = {11, 10, 14, 15};\n')
geoFile.write('Recombine Surface{8};\n')
geoFile.write('\n')

geoFile.write('Line Loop(%s) = {%s, %s, %s, %s};\n' %(9, 17, 21, 24, 20))
#Surface
geoFile.write('Plane Surface(9) = {9};\n')
geoFile.write('Transfinite Surface{9} = {11, 12, 16, 15};\n')
geoFile.write('Recombine Surface{9};\n')
geoFile.write('\n')

geoFile.write('surfaceVector1[] = Extrude {0, 0, %s} {\n'
'Surface{1, 2, 3, 4, 6, 7, 8, 9};\n'
'Recombine;\n'
'Layers{{%s, %s},{1-(%s/%s),1}};\n'
'};\n'
'\n'%( -z_translation_area, 2.0, 9.0, 22.5, z_translation_area ))
geoFile.write('\n')


geoFile.write('surfaceVector2[] = Extrude {0, 0, %s} {\n'
'Surface{5};\n'
'Recombine;\n'
'Layers{{%s, %s},{1-(%s/%s),1}};\n'
'};\n'
'\n'%( -z_translation_area, 2.0, 9.0, 22.5, z_translation_area ))
geoFile.write('\n')


geoFile.write('surfaceVector3[] = Extrude {0, 0, %s} {\n'
'Surface{5};\n'
'Recombine;\n'
'Layers{{%s, %s},{%s,1}};\n'
'};\n'
'\n'%(z_translation_landfill, 1.0, 19.0, 1/z_translation_landfill ))
geoFile.write('\n')
'''
geoFile.write('surfaceVector2[] = Extrude {0, 0, %s} {\n'
'Surface{5};\n'
'Recombine;\n'
'Layers{{%s, %s, %s, %s},{%s/%s, %s/%s, %s/%s, 1}};\n'
'};\n'
'\n'%(z_translation_area_landfill, 7.0, 2.0, 1.0, 19.0, 17.5, z_translation_area_landfill, z_translation_area, z_translation_area_landfill, 1 + z_translation_area, z_translation_area_landfill ))
geoFile.write('\n')
'''

'''
#left area finer grid
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(1, 0.0, 0.0, 0.0, 100.0 ))   
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(2, length_finer_grid, 0.0, 0.0, 100.0 ))   
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(3, length_finer_grid, w_a, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(4, 0.0, w_a, 0.0, 100.0 ))
geoFile.write('\n')

geoFile.write('Line(%s) = {%s, %s};\n' %(1, 1, 2))
geoFile.write('Transfinite Line{%s} = %s;\n' %(1, length_finer_grid/grid_l_l +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(2, 2, 3))
geoFile.write('Transfinite Line{%s} = %s;\n' %(2, w_a/grid_w_l +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(3, 3, 4))
geoFile.write('Transfinite Line{%s} = %s;\n' %(3, length_finer_grid/grid_l_l +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(4, 4, 1))
geoFile.write('Transfinite Line{%s} = %s;\n' %(4, w_a/grid_w_l +1))
geoFile.write('\n')
#lIdx = 5
#line loop corner points
geoFile.write('Line Loop(%s) = {%s, %s, %s, %s};\n' %(1, 1, 2, 3, 4))
#Surface
geoFile.write('Plane Surface(1) = {1};\n')
geoFile.write('Transfinite Surface{1} = {1};\n')
geoFile.write('Recombine Surface{1};\n')
geoFile.write('\n')


#right area wider grid
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(5, length_finer_grid, 0.0, 0.0, 100.0 ))   
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(6, l_a, 0.0, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(7, l_a, w_a, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(8, length_finer_grid, w_a, 0.0, 100.0 ))
geoFile.write('\n')

geoFile.write('Line(%s) = {%s, %s};\n' %(5, 5, 6))
geoFile.write('Transfinite Line{%s} = %s;\n' %(5, length_wider_grid/grid_l_a +1  ))
geoFile.write('Line(%s) = {%s, %s};\n' %(6, 6, 7))
geoFile.write('Transfinite Line{%s} = %s;\n' %(6, w_a/grid_w_l +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(7, 7, 8))
geoFile.write('Transfinite Line{%s} = %s;\n' %(7, length_wider_grid/grid_l_a +1 ))
geoFile.write('Line(%s) = {%s, %s};\n' %(8, 8, 5))
geoFile.write('Transfinite Line{%s} = %s;\n' %(8, w_a/grid_w_l +1))
geoFile.write('\n')
#lIdx = 5
#line loop corner points
geoFile.write('Line Loop(%s) = {%s, %s, %s, %s};\n' %(2, 5, 6, 7, 8))
#Surface
geoFile.write('Plane Surface(2) = {2};\n')
geoFile.write('Transfinite Surface{2} = {2};\n')
geoFile.write('Recombine Surface{2};\n')
geoFile.write('\n')


#landfill

geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(9, l_t_l, w_t_l, 0.0, 100.0 ))
geoFile.write('Point{9} In Surface{1};\n')
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(10, l_t_l + l_l, w_t_l, 0.0, 100.0 ))
geoFile.write('Point{10} In Surface{1};\n')
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(11, l_t_l + l_l, w_t_l + w_l, 0.0, 100.0 ))
geoFile.write('Point{11} In Surface{1};\n')
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(12, l_t_l, w_t_l + w_l, 0.0, 100.0 ))
geoFile.write('Point{12} In Surface{1};\n')
geoFile.write('\n')

geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(13, l_t_l, w_t_l, z_translation_area, 50.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(14, l_t_l + l_l, w_t_l, z_translation_area, 50.0))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(15, l_t_l + l_l, w_t_l + w_l, z_translation_area, 50.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(16, l_t_l, w_t_l + w_l, z_translation_area, 50.0 ))
geoFile.write('\n')


geoFile.write('Line(%s) = {%s, %s};\n' %(9, 13, 14))
geoFile.write('Transfinite Line{%s} = %s;\n' %(9, l_l/grid_l_l +1))
geoFile.write('Line(%s) = {%s, %s};\n' %(10, 14, 15))
geoFile.write('Transfinite Line{%s} = %s;\n' %(10, w_l/grid_w_l +1))
geoFile.write('Line(%s) = {%s, %s};\n' %(11, 15, 16))
geoFile.write('Transfinite Line{%s} = %s;\n' %(11, l_l/grid_l_l +1))
geoFile.write('Line(%s) = {%s, %s};\n' %(12, 16, 13))
geoFile.write('Transfinite Line{%s} = %s;\n' %(12, w_l/grid_w_l +1))
geoFile.write('\n')
geoFile.write('Line Loop(%s) = {%s, %s, %s, %s};\n' %(3, 9, 10, 11, 12))
#Surface
geoFile.write('Plane Surface(3) = {3};\n')
geoFile.write('Transfinite Surface{3} = {3};\n')
geoFile.write('Recombine Surface{3};\n')
geoFile.write('\n')

#landfill

geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(9, l_t_l, w_t_l, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(10, l_t_l + l_l, w_t_l, 0.0, 100.0))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(11, l_t_l + l_l, w_t_l + w_l, 0.0, 100.0 ))
geoFile.write('Point(%s) = {%s, %s, %s , %s};\n' %(12, l_t_l, w_t_l + w_l, 0.0, 100.0 ))
geoFile.write('\n')


geoFile.write('Line(%s) = {%s, %s};\n' %(9, 9, 10))
geoFile.write('Transfinite Line{%s} = %s;\n' %(9, l_l/grid_l_l +1))
geoFile.write('Line(%s) = {%s, %s};\n' %(10, 10, 11))
geoFile.write('Transfinite Line{%s} = %s;\n' %(10, w_l/grid_w_l +1))
geoFile.write('Line(%s) = {%s, %s};\n' %(11, 11, 12))
geoFile.write('Transfinite Line{%s} = %s;\n' %(11, l_l/grid_l_l +1))
geoFile.write('Line(%s) = {%s, %s};\n' %(12, 12, 9))
geoFile.write('Transfinite Line{%s} = %s;\n' %(12, w_l/grid_w_l +1))
geoFile.write('\n')
geoFile.write('Line Loop(%s) = {%s, %s, %s, %s};\n' %(3, 9, 10, 11, 12))
#Surface
geoFile.write('Plane Surface(3) = {3};\n')
geoFile.write('Transfinite Surface{3} = {3};\n')
geoFile.write('Recombine Surface{3};\n')
geoFile.write('\n')


geoFile.write('surfaceVector1[] = Extrude {0, 0, %s} {\n'
'Surface{1};\n'
'Recombine;\n'
'Layers{{%s, %s},{1-(%s/%s),1}};\n'
'};\n'
'\n'%(- z_translation_area, 2.0, 7.0, 17.5, z_translation_area ))
geoFile.write('\n')

geoFile.write('surfaceVector1[] = Extrude {0, 0, %s} {\n'
'Surface{1};\n'
'Recombine;\n'
'Layers{{%s, %s, %s, %s},{%s/%s, %s/%s, %s/%s, 1}};\n'
'};\n'
'\n'%(z_translation_area_landfill, 7.0, 2.0, 1.0, 19.0, 17.5, z_translation_area_landfill, z_translation_area, z_translation_area_landfill, 1 + z_translation_area, z_translation_area_landfill ))
geoFile.write('\n')


geoFile.write('surfaceVector2[] = Extrude {0, 0, %s} {\n'
'Surface{2};\n'
'Recombine;\n'
'Layers{{%s, %s},{1-(%s/%s),1}};\n'
'};\n'
'\n'%(- z_translation_area, 2.0, 7.0, 17.5, z_translation_area ))
geoFile.write('\n')


geoFile.write('surfaceVector3[] = Extrude {0, 0, %s} {\n'
'Surface{3};\n'
'Recombine;\n'
'Layers{{%s, %s},{%s,1}};\n'
'};\n'
'\n'%(z_translation_landfill, 1.0, 19.0, 1/z_translation_landfill ))
geoFile.write('\n')


geoFile.write('surfaceVector3[] = Extrude {0, 0, %s} {\n'
'Surface{3};\n'
'Recombine;\n'
'Layers{{%s, %s, %s, %s},{%s/%s, %s/%s, %s/%s, 1}};\n'
'};\n'
'\n'%(z_translation_area_landfill, 7.0, 2.0, 1.0, 19.0, 17.5, z_translation_area_landfill, z_translation_area, z_translation_area_landfill, 1 + z_translation_area, z_translation_area_landfill ))
geoFile.write('\n')
'''






geoFile.write('/* surfaceVector contains in the following order:\n'
'[0] - front surface (opposed to source surface)\n'
'[1] - extruded volume\n'
'[2] - bottom surface (belonging to 1st line in "Line Loop (6)")\n'
'[3] - right surface (belonging to 2nd line in "Line Loop (6)")\n'
'[4] - top surface (belonging to 3rd line in "Line Loop (6)")\n'
'[5] - left surface (belonging to 4th line in "Line Loop (6)") */\n'
'\n')

#geoFile.write('Physical Volume(7) = {surfaceVector1[], surfaceVector2[], surfaceVector3[]};\n'
#'\n')
#geoFile.write('SetFactory("OpenCASCADE"); \n')

#geoFile.write(' v() = BooleanUnion{Surface{1};} { Surface{2:3};  };\n'
#'\n')




geoFile.close()
#execute gmsh with created file
subprocess.call(['gmsh','-format','msh2','-3',filenameGeo])
#watch the created Msh file
filenameMsh = filenameOutput+'.msh'
#subprocess.call(['gmsh', filenameMsh])

StartWord = '$Nodes'
EndWord = '$EndNodes'
begin = False
readVertices = False
#TransMatrixInv = np.linalg.inv(TransMatrix)
for line in fileinput.input(filenameMsh, inplace = True):
    if line.startswith(EndWord):
        #end of transformation
        begin = False
        print(line.strip('\n'))
    elif line.startswith(StartWord):
        #start of transformation
        print(line.strip('\n'))
        begin = True
    elif begin:
       if not readVertices:
           #first after startword is the number of nodes, dont want to tranform this
           print(line.strip('\n'))
           readVertices = True
           continue
       elif readVertices:
           #line is a string with no separators and a \n at the end, need to convert it for the calculation
           newline = line.strip('\n').split(' ')
           #convert it to a array with floats
           vals1 = np.array(newline).astype(np.float)
           #the inverse transformation
           vals1[3] = vals1[3] + (height - vals1[1]*delta_height) #(height- (delta_height*vals1[3] #vals1[3] + deltah*(1-vals1[2]/w_a)
           #vals1[1:3] = np.dot(vals1[1:3],TransMatrixInv) + Points[0]
           vals1[1] = vals1[1] + (vals1[3] - (height - vals1[1]*delta_height)) *cot_angle_translation
           vals1 = vals1.astype(np.str)
           #set transformed points
           newline[1:4] = vals1[1:4]
           newline = ' '.join(newline)
           #replace old line with new one
           print(line.replace(line, newline))
    else:
        #write the other stuff in the file
        print(line.strip('\n'))

#calculate the normalVector
#NormalVector = np.array([0, 1])
#RotNormalVector = np.dot(NormalVector, TransMatrixInv)
#Check the rotated normal Vector for the Boundary markers
#print(RotNormalVector)
#watch the created changed Msh file
#subprocess.call(['gmsh', filenameMsh])






