Point(1) = {0.0, 0.0, 0.0 , 100.0};
Point(2) = {0.0, 300.0, 0.0 , 100.0};
Point(3) = {0.0, 600.0, 0.0 , 100.0};
Point(4) = {0.0, 900.0, 0.0 , 100.0};
Point(5) = {300.0, 0.0, 0.0 , 100.0};
Point(6) = {300.0, 300.0, 0.0 , 100.0};
Point(7) = {300.0, 600.0, 0.0 , 100.0};
Point(8) = {300.0, 900.0, 0.0 , 100.0};
Point(9) = {600.0, 0.0, 0.0 , 100.0};
Point(10) = {600.0, 300.0, 0.0 , 100.0};
Point(11) = {600.0, 600.0, 0.0 , 100.0};
Point(12) = {600.0, 900.0, 0.0 , 100.0};
Point(13) = {4500.0, 0.0, 0.0 , 100.0};
Point(14) = {4500.0, 300.0, 0.0 , 100.0};
Point(15) = {4500.0, 600.0, 0.0 , 100.0};
Point(16) = {4500.0, 900.0, 0.0 , 100.0};
Line(1) = {1, 2};
Transfinite Line{1} = 3.0;
Line(2) = {3, 2};
Transfinite Line{2} = 11.0;
Line(3) = {3, 4};
Transfinite Line{3} = 3.0;
Line(4) = {5, 1};
Transfinite Line{4} = 3.0;
Line(5) = {2, 6};
Transfinite Line{5} = 3.0;
Line(6) = {7, 3};
Transfinite Line{6} = 3.0;
Line(7) = {4, 8};
Transfinite Line{7} = 3.0;
Line(8) = {6, 5};
Transfinite Line{8} = 3.0;
Line(9) = {6, 7};
Transfinite Line{9} = 11.0;
Line(10) = {8, 7};
Transfinite Line{10} = 3.0;
Line(11) = {5, 9};
Transfinite Line{11} = 11.0;
Line(12) = {10, 6};
Transfinite Line{12} = 11.0;
Line(13) = {7, 11};
Transfinite Line{13} = 11.0;
Line(14) = {12, 8};
Transfinite Line{14} = 11.0;
Line(15) = {9, 10};
Transfinite Line{15} = 3.0;
Line(16) = {11, 10};
Transfinite Line{16} = 11.0;
Line(17) = {11, 12};
Transfinite Line{17} = 3.0;
Line(18) = {13, 9};
Transfinite Line{18} = 27.0;
Line(19) = {10, 14};
Transfinite Line{19} = 27.0;
Line(20) = {15, 11};
Transfinite Line{20} = 27.0;
Line(21) = {12, 16};
Transfinite Line{21} = 27.0;
Line(22) = {14, 13};
Transfinite Line{22} = 3.0;
Line(23) = {14, 15};
Transfinite Line{23} = 11.0;
Line(24) = {16, 15};
Transfinite Line{24} = 3.0;

Line Loop(1) = {1, 5, 8, 4};
Plane Surface(1) = {1};
Transfinite Surface{1} = {1, 2, 6, 5};
Recombine Surface{1};

Line Loop(2) = {2, 5, 9, 6};
Plane Surface(2) = {2};
Transfinite Surface{2} = {3, 2, 6, 7};
Recombine Surface{2};

Line Loop(3) = {3, 7, 10, 6};
Plane Surface(3) = {3};
Transfinite Surface{3} = {3, 4, 8, 7};
Recombine Surface{3};

Line Loop(4) = {8, 11, 15, 12};
Plane Surface(4) = {4};
Transfinite Surface{4} = {6, 5, 9, 10};
Recombine Surface{4};

Line Loop(5) = {9, 13, 16, 12};
Plane Surface(5) = {5};
Transfinite Surface{5} = {6, 7, 11, 10};
Recombine Surface{5};

Line Loop(6) = {10, 13, 17, 14};
Plane Surface(6) = {6};
Transfinite Surface{6} = {8, 7, 11, 12};
Recombine Surface{6};

Line Loop(7) = {15, 19, 22, 18};
Plane Surface(7) = {7};
Transfinite Surface{7} = {9, 10, 14, 13};
Recombine Surface{7};

Line Loop(8) = {16, 19, 23, 20};
Plane Surface(8) = {8};
Transfinite Surface{8} = {11, 10, 14, 15};
Recombine Surface{8};

Line Loop(9) = {17, 21, 24, 20};
Plane Surface(9) = {9};
Transfinite Surface{9} = {11, 12, 16, 15};
Recombine Surface{9};

surfaceVector1[] = Extrude {0, 0, -27.311227114629528} {
Surface{1, 2, 3, 4, 6, 7, 8, 9};
Recombine;
Layers{{2.0, 9.0},{1-(22.5/27.311227114629528),1}};
};


surfaceVector2[] = Extrude {0, 0, -27.311227114629528} {
Surface{5};
Recombine;
Layers{{2.0, 9.0},{1-(22.5/27.311227114629528),1}};
};


surfaceVector3[] = Extrude {0, 0, 19.862710628821475} {
Surface{5};
Recombine;
Layers{{1.0, 19.0},{0.05034559575916923,1}};
};


/* surfaceVector contains in the following order:
[0] - front surface (opposed to source surface)
[1] - extruded volume
[2] - bottom surface (belonging to 1st line in "Line Loop (6)")
[3] - right surface (belonging to 2nd line in "Line Loop (6)")
[4] - top surface (belonging to 3rd line in "Line Loop (6)")
[5] - left surface (belonging to 4th line in "Line Loop (6)") */

