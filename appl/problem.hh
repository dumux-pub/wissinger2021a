// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPNCMinTests
 * \brief 
 */
#ifndef DUMUX_2PNCMIN_RADTEST_PROBLEM_HH
#define DUMUX_2PNCMIN_RADTEST_PROBLEM_HH

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>

#include <dumux/porousmediumflow/2pncmin/model.hh>
#include <dumux/material/solidsystems/compositionalsolidphase.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2pnc/model.hh>

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/h2oaircs.hh>

#include "spatialparams.hh"
#include <dumux/flux/maxwellstefanslaw.hh>

#if FORCHHEIMER
#include <dumux/flux/forchheimerslaw.hh>
#endif

namespace Dumux {

template <class TypeTag>
class TwoPNCMinRadTestProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct TwoPNCMinRadTest { using InheritsFrom = std::tuple<TwoPNCMin>; };
struct TwoPNCMinRadTestBox { using InheritsFrom = std::tuple<TwoPNCMinRadTest, BoxModel>; };
struct TwoPNCMinRadTestCCTpfa { using InheritsFrom = std::tuple<TwoPNCMinRadTest, CCTpfaModel>; };
struct TwoPNCMinRadTestCCMpfa { using InheritsFrom = std::tuple<TwoPNCMinRadTest, CCMpfaModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPNCMinRadTest> { using type = Dune::UGGrid<3>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPNCMinRadTest> { using type = TwoPNCMinRadTestProblem<TypeTag>; };

// Specialize the fluid system type for this type tag
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPNCMinRadTest>
{
    using Scalar = GetPropType<TypeTag, Scalar>;
    using type = FluidSystems::H2OAirCs<Scalar, Components::SimpleH2O<Scalar> >;
};

template<class TypeTag>
struct SolidSystem<TypeTag, TTag::TwoPNCMinRadTest>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    static constexpr int numInertComponents = 1;
    using type = SolidSystems::CompositionalSolidPhase<Scalar, numInertComponents>;
};


// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPNCMinRadTest>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = TwoPNCMinRadTestSpatialParams<GridGeometry, Scalar>;
};

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::TwoPNCMinRadTest> { static constexpr bool value = true; };

//! Here we set FicksLaw or TwoPNCDiffusionsLaw
template<class TypeTag>
struct MolecularDiffusionType<TypeTag, TTag::TwoPNCMinRadTest> { using type = FicksLaw<TypeTag>; };

//! Set the default formulation to pw-Sn: This can be over written in the problem.
template<class TypeTag>
struct Formulation<TypeTag, TTag::TwoPNCMinRadTest>
{ static constexpr auto value = TwoPFormulation::p0s1; };

} // end namespace Properties


/*!
 * \ingroup TwoPNCTests
 * \brief 
 */
template <class TypeTag>
class TwoPNCMinRadTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using PcKrSwCurve = typename ParentType::SpatialParams::PcKrSwCurve;
    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;

    enum {
        dim = GridView::dimension,
        
        // copy some indices for convenience
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,
        bothPhases = Indices::bothPhases,
        firstPhaseOnly = Indices::firstPhaseOnly,
        
        //Indices of the phases
        liquidPhaseIdx = FluidSystem::liquidPhaseIdx,
        gasPhaseIdx = FluidSystem::gasPhaseIdx,
        
        //component indices
        precipCs137Idx = FluidSystem::numComponents,

        // component indices
        H2OIdx = FluidSystem::H2OIdx,
        AirIdx = FluidSystem::AirIdx,
        Cs137Idx =  FluidSystem::Caesium137Idx,

        Cs137liqIdx = Cs137Idx + FluidSystem::liquidPhaseIdx,
        Cs137gasIdx = Cs137Idx + FluidSystem::gasPhaseIdx,
        // index of the solid phase
        sPhaseIdx = SolidSystem::comp0Idx,

        // equation indices
        conti0EqIdx = Indices::conti0EqIdx + H2OIdx,
        contiAirEqIdx = conti0EqIdx + AirIdx,
        contiCs137EqIdx = conti0EqIdx + FluidSystem::Caesium137Idx,
        precipCs137EqIdx = Indices::conti0EqIdx + FluidSystem::numComponents
    };

    //! Property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = getPropValue<TypeTag, Properties::UseMoles>();

public:
    TwoPNCMinRadTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry) {
        // initialize the tables of the fluid system
        FluidSystem::init();

        name_ = getParam<std::string>("Problem.Name");
        injectionMass_ = getParam<Scalar>("Problem.InjectionMass");

        // stating in the console whether mole or mass fractions are used
        if(useMoles)
        { std::cout<<"problem uses mole-fractions"<<std::endl; }
        else
        { std::cout<<"problem uses mass-fractions"<<std::endl; }
    }
    
    void setTime( Scalar time )
    {
        time_ = time;
    }
    
    void setTimeStepSize( Scalar timeStepSize )
    {
        timeStepSize_ = timeStepSize;
    }
    

    static constexpr auto numComponents = GetPropType<TypeTag, Properties::ModelTraits>::numFluidComponents();

    /*!
     * \brief Appends all quantities of interest which can be derived
     *        from the solution of the current time step to the VTK
     *        writer.
     */
    template<class VTKWriter>
    void addFieldsToWriter(VTKWriter& vtk)
    {
        const auto numDofs = this->gridGeometry().numDofs();
        vtkBqPerKg_.resize(numDofs, 0.0);
        vtk.addField(vtkBqPerKg_, "BqPerKg");
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter.
     *
     * Function is called by the output module on every write.
     */
    void updateVtkFields(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto elemSol = elementSolution(element, curSol, this->gridGeometry());
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                const auto dofIdxGlobal = scv.dofIndex();
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);

                for (int compIdx = 0; compIdx < numComponents; ++compIdx)
                {
                    if (!FluidSystem::isRadioactive(compIdx))
                        continue;
                    vtkBqPerKg_[dofIdxGlobal] += volVars.massFraction(liquidPhaseIdx, compIdx)
                                                  * FluidSystem::specificActivity(compIdx) + SolidSystem::density(compIdx) * volVars.solidVolumeFraction(compIdx) * SolidSystem::specificActivity(compIdx);
                }
            }
        }
    }

    NumEqVector source(const Element &element,
                   const FVElementGeometry& fvGeometry,
                   const ElementVolumeVariables& elemVolVars,
                   const SubControlVolume &scv) const
    {
        NumEqVector source(0.0);

        const auto& volVars = elemVolVars[scv];
        
        std::vector<Scalar> massEquil_(SolidSystem::numComponents-SolidSystem::numInertComponents);
        for(int compIdx=0; compIdx < (SolidSystem::numComponents-SolidSystem::numInertComponents); ++compIdx)
        {
            if(volVars.solidVolumeFraction(compIdx) <= 0)
            {
                massEquil_[compIdx] = volVars.moleFraction(liquidPhaseIdx, compIdx + 2) * FluidSystem::kdValue(compIdx +2) * (1 - volVars.porosity())
                                      / ( FluidSystem::molarMass(compIdx + 2) * timeStepSize_);

                if( abs(massEquil_[compIdx] * timeStepSize_ /  FluidSystem::molarDensity(compIdx +2)) > abs(volVars.moleFraction(liquidPhaseIdx, compIdx +2 )))
                {
                    massEquil_[compIdx] = volVars.moleFraction(liquidPhaseIdx, compIdx +2 ) * FluidSystem::molarDensity(compIdx +2 ) / timeStepSize_;
                }
                else if(abs(massEquil_[compIdx] * timeStepSize_ /  FluidSystem::molarDensity(compIdx +2)) < 1e-15)
                {
                    massEquil_[compIdx] = 0.0;
                }
                source[conti0EqIdx + 2 + compIdx] =  -abs(massEquil_[compIdx]);
                source[precipCs137EqIdx + compIdx] = abs(massEquil_[compIdx]);
            }
                
            else
            {
                massEquil_[compIdx] = volVars.solidVolumeFraction(compIdx) * volVars.porosity() *  volVars.saturation(liquidPhaseIdx) 
                                        / ( FluidSystem::molarMass(compIdx +2) * FluidSystem::kdValue(compIdx +2) * timeStepSize_);

                if(abs(massEquil_[compIdx] * timeStepSize_ / FluidSystem::molarDensity(compIdx +2 ))  > abs(volVars.solidVolumeFraction(compIdx)))
                {
                    massEquil_[compIdx] = volVars.solidVolumeFraction(compIdx) * FluidSystem::molarDensity(compIdx +2 )  / timeStepSize_;
                }
                source[conti0EqIdx + 2 + compIdx] =  abs(massEquil_[compIdx]);
                source[precipCs137EqIdx + compIdx] = -abs(massEquil_[compIdx]);
            }
        }
        return source;
    }
   

     /* \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns the problem name
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Returns the temperature [K]
     */
    Scalar temperature() const
    { return 273.15 + 10; }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypes(const Element &element, 
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        const auto& globalPos =  element.geometry().center();
        if((onLeftBoundary_(scvf) && globalPos[0] < 100 && isSaturated_(globalPos)) || (onRightBoundary_(scvf) && globalPos[0] > 4400 && isSaturated_(globalPos)))
        {
            values.setAllDirichlet();
        }
            
        else
            values.setAllNeumann();

        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment.
     *
     * For this method, the \a priVars parameter stores the mass flux
     * in normal direction of each component. Negative values mean influx.
     * \param globalPos The global position
     *
     * \note The units must be according to either using mole or mass fractions (mole/(m^2*s) or kg/(m^2*s)).
     */
    template<class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        const auto& globalPos = scvf.ipGlobal();
        
        if (onUpperBoundary_(scvf) && isLandfill_(globalPos))
        {
           values[conti0EqIdx] = injectionMass_;            
            
            /*const auto& volVars = elemVolVars[scvf.insideScvIdx()];
            const Scalar pW = volVars.pressure(waterPhaseIdx);
            const Scalar pN = volVars.pressure(gasPhaseIdx);
            const Scalar satW = volVars.saturation(waterPhaseIdx);
            const Scalar satN = volVars.saturation(gasPhaseIdx);
            const Scalar kf = 0.00571033287478;
            values[conti0EqIdx] = satW * (pW-1e5) * kf;
            if (pN>1.e5) 
            { 
                values[conti1EqIdx] = satN * (pN-1.e5) * kf; 
            }
            values[conti0EqIdx] = injectionMass_;*/
        }

        return values;
    }
    // \}

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichlet(const Element &element,
                               const SubControlVolumeFace &scvf) const
    {
        const auto& globalPos = scvf.ipGlobal();
        PrimaryVariables values(0.0);
        values.setState(bothPhases);
        Scalar densityW = 1000.0;
        values[Cs137Idx] = 0.0;

        const Scalar sw = 1.0;
        values[pressureIdx] = 1e5 + densityW * 9.81 * (getWaterHeight_(globalPos) - globalPos[dim-1]);
        values[switchIdx] = 1.0 - sw;
        values[precipCs137Idx] = 0.0;
    

        return values;
    }

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initial(const Element& element) const
    {
        PrimaryVariables values(0.0);
        const auto& globalPos =  element.geometry().center();
        values.setState(bothPhases);
        Scalar densityW = 1000.0;
        values[Cs137Idx] = 0.0;

        if(isLandfill_(globalPos))
        {
            const Scalar sw = 0.01;
            const Scalar pc = this->spatialParams().fluidMatrixInteraction(element).pc(sw);
            values[pressureIdx] = 1e5 - pc;
            values[switchIdx] = 1.0 - sw;
            values[precipCs137Idx] = 1e-6;
        }

        else if(isSaturated_(globalPos))
        {
            const Scalar sw = 1.0;
            values[pressureIdx] = 1e5 + densityW * 9.81 * (getWaterHeight_(globalPos) - globalPos[dim-1]);
            values[switchIdx] = 1.0 - sw;
            values[precipCs137Idx] = 0.0;
        }

        else
        {
            const Scalar sw = 0.01;
            const Scalar pc = this->spatialParams().fluidMatrixInteraction(element).pc(sw);
            values[pressureIdx] = 1e5 - pc;
            values[switchIdx] = 1.0 - sw;
            values[precipCs137Idx] = 0.0;
        }

        return values;
    }
    // \}

private:
    bool onUpperBoundary_(const SubControlVolumeFace& scvf) const
    {
        return scvf.unitOuterNormal()[dim-1] > 0.5;
    }

    bool onLowerBoundary_(const SubControlVolumeFace& scvf) const
    {
        return scvf.unitOuterNormal()[dim-1] < -0.5;
    }

    bool onLeftBoundary_(const SubControlVolumeFace& scvf) const
    {
        return scvf.unitOuterNormal()[0] < -0.5;
    }

    bool onRightBoundary_(const SubControlVolumeFace& scvf) const
    {
        return scvf.unitOuterNormal()[0] > 0.5;
    }

    bool onFrontBoundary_(const SubControlVolumeFace& scvf) const
    {
        return scvf.unitOuterNormal()[1] < -0.5;
    }

    bool onBackBoundary_(const SubControlVolumeFace& scvf) const
    {
        return scvf.unitOuterNormal()[1] > 0.5;
    }

    //height of the area without the landfill
    Scalar getHeightArea_(const GlobalPosition& globalPos) const
    {
        return 0 + height_ * (lengthX_ - globalPos[0]) / lengthX_;
    }

    Scalar getWaterHeight_(const GlobalPosition& globalPos) const
    {
        return -5 + height_ * (lengthX_ - globalPos[0]) / lengthX_;
    }

    bool isLandfill_ (const GlobalPosition& globalPos) const
    {
        return globalPos[dim-1] > 0 + height_ * (lengthX_ - globalPos[0]) / lengthX_ + eps_;
    }

    bool isSaturated_ (const GlobalPosition& globalPos) const
    {
        return globalPos[dim-1] <=  -5 + height_ * (lengthX_ - globalPos[0]) / lengthX_ + eps_;
    }

    static constexpr Scalar eps_ = 1e-6;

    //Scalar time_;
    Scalar injectionMass_;
    static constexpr Scalar height_ = 530;
    static constexpr Scalar lengthX_ = 4500;
    Scalar timeStepSize_ = 0.0;
    Scalar time_ = 0.0;

    std::vector<Scalar> K_, vtkBqPerKg_, vtkK_;
    
    std::string name_;
    Scalar radMoleFraction_;
};

} // end namespace Dumux

#endif
