// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPNCMinTests
 * \brief Definition of the spatial parameters for the Radioactive 2PNCMinTest problem
 * using the two phase n-components model.
 */

#ifndef DUMUX_TWOPNC_RADIOACTIVETEST_SPATIAL_PARAMS_HH
#define DUMUX_TWOPNC_RADIOACTIVETEST_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/properties.hh>
#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/brookscorey.hh>

namespace Dumux {
/*!
 * \ingroup TwoPNCMinTests
 * \brief Definition of the spatial parameters for the Radioactive 2PNCMinTest problem
 * using the two phase n-components model.
 */
template<class GridGeometry, class Scalar>
class TwoPNCMinRadTestSpatialParams
: public FVSpatialParams<GridGeometry, Scalar,
                         TwoPNCMinRadTestSpatialParams<GridGeometry, Scalar>>
{
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using ThisType = TwoPNCMinRadTestSpatialParams<GridGeometry, Scalar>;
    using ParentType = FVSpatialParams<GridGeometry, Scalar, ThisType>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

public:
    using PcKrSwCurve = FluidMatrix::BrooksCoreyDefault<Scalar>;
    using PermeabilityType = Scalar;

    TwoPNCMinRadTestSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    , K_(gridGeometry->gridView().size(0), 0.0)
    , entryPressure_(gridGeometry->gridView().size(0), 0.0)
    , globalPos_(gridGeometry->gridView().size(0))
    {
        permeability_ = getParam<Scalar>("SpatialParams.K", 1e-11);
        PcEInit_ = getParam<Scalar>("SpatialParams.BrooksCoreyPcEntry", 1e3);
        swr_ = getParam<Scalar>("SpatialParams.Swr", 0.0);
        snr_ = getParam<Scalar>("SpatialParams.Snr", 0.0);
        lambda_ = getParam<Scalar>("SpatialParams.BrooksCoreyLambda", 2.0);

        permField_ = getParam<bool>("SpatialParams.permField", false);

        if(!permField_)
        {
            for (const auto& element : elements(this->gridGeometry().gridView()))
            {
                auto eIdx = this->gridGeometry().elementMapper().index(element);
                globalPos_[eIdx] =  element.geometry().center();
                if (isLandfill_(globalPos_[eIdx]))
                {
                    K_[eIdx] =  4525/(7.5*1000000 * 3600 * 24* 365); // assumption: K_f = 4525/(3600 * 24* 365)
                }
                else
                {
                    K_[eIdx] = 2602/(7.5*1000000 * 3600 * 24* 365); // assumption: K_f = 2602/(3600 * 24* 365)
                }
                entryPressure_[eIdx]= PcEInit_ ;
            }
        }
        else
        {
            //reading permeability from an external created file
            std::string permDataFile = getParam<std::string>("SpatialParams.permFile");
            std::ifstream permFile(permDataFile);
            if (!permFile.good())
            {
                DUNE_THROW(Dune::IOError, "Reading from file: "
                        << permDataFile << " failed." << std::endl);
            }
            std::string line;
            std::getline(permFile, line);

            Scalar trash, dataValue;
            for (const auto& element : elements(this->gridGeometry().gridView()))
            {
                auto eIdx = this->gridGeometry().elementMapper().index(element);
                globalPos_[eIdx] =  element.geometry().center(); 
                if (isLandfill_(globalPos_[eIdx]))
                {
                    std::getline(permFile, line);
                    std::istringstream curLine(line);
                    if (dim == 1)
                        curLine >> trash >> dataValue;
                    else if (dim == 2)
                        curLine >> trash >> trash >> dataValue;
                    else if (dim == 3)
                        curLine >> trash >> trash >> trash >> dataValue;
                    else
                        DUNE_THROW(Dune::InvalidStateException, "Invalid dimension " << dim);

                    K_[eIdx] = permeability_ * std::pow(10, dataValue);
                }
                else
                {
                    K_[eIdx] = 2602/(7.5*1000000 * 3600 * 24* 365); //assumption: K_f = 2602/(3600 * 24* 365)
                }

                // Leverett after Saadatpoor, 2009, Effect of capillary heterogeneity
                entryPressure_[eIdx]= PcEInit_* std::sqrt(permeability_ / K_[eIdx]);
            }
        }
    }

    //! get the permeability field for output
    const std::vector<Scalar>& getPermField() const
    { 
        return K_; 
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     *        In this test, we use element-wise distributed permeabilities.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return The permeability
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    { 
        return K_[scv.dofIndex()]; 
    }

    /*!
     *  \brief Defines the volume fraction of the inert component
     *
     *  \param globalPos The global position in the domain
     *  \param compIdx The index of the inert solid component
     */
    template<class SolidSystem>
    Scalar inertVolumeFractionAtPos(const GlobalPosition& globalPos, int compIdx) const
    { return 1.0-porosityAtPos(globalPos); }


    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param globalPos The global position
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        if (isLandfill_(globalPos))
            return 0.46;
        else
            return 0.43;
    }
    
   /* Scalar saturation(const Element &element,
                      const SubControlVolume &scv) const
    
    {
       return 0.5;
    } */

    template<class ElementSolution>
    auto fluidMatrixInteraction(const Element& element,
                                const SubControlVolume& scv,
                                const ElementSolution& elemSol) const
    {
        typename PcKrSwCurve::BasicParams params(entryPressure_[scv.dofIndex()], lambda_);
        typename PcKrSwCurve::EffToAbsParams effToAbsParams(swr_, snr_);
        return makeFluidMatrixInteraction(PcKrSwCurve(params, effToAbsParams));
    }

    /*!
     * \brief Returns the fluid-matrix interaction law an element
     *
     * \param element The current finite element
     */
    auto fluidMatrixInteraction(const Element& element) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        //const auto& globalPos = globalPos_[eIdx];

        typename PcKrSwCurve::BasicParams params(entryPressure_[eIdx], lambda_);
        typename PcKrSwCurve::EffToAbsParams effToAbsParams(swr_, snr_);
        return makeFluidMatrixInteraction(PcKrSwCurve(params, effToAbsParams));
    }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \param globalPos The global position
     * \return The wetting phase index
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { 
        return FluidSystem::phase0Idx; 
    }

private:
    /*bool isLandfill_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] > 22.5 - eps_;
    } */
    //height of the area without the landfill
    Scalar getHeightArea_(const GlobalPosition& globalPos) const
    {
        return 0 + height_ * (lengthX_ - globalPos[0]) / lengthX_ ;
    }
    
    bool isSaturated_ (const GlobalPosition& globalPos) const
    {
        return globalPos[dim-1] <=  -5 + height_ * (lengthX_ - globalPos[0]) / lengthX_;
    }
    
    bool isLandfill_ (const GlobalPosition& globalPos) const
    {
        return globalPos[dim-1] > 0 + height_ * (lengthX_ - globalPos[0]) / lengthX_;
    }
    
    
   
    Scalar permeability_;
    Scalar PcEInit_;
    Scalar swr_;
    Scalar snr_;
    Scalar lambda_;

    bool permField_;
    
    std::vector<Scalar> K_;
    std::vector<Scalar> entryPressure_;
    std::vector<GlobalPosition> globalPos_;

    static constexpr Scalar eps_ = 1.5e-7;
    static constexpr Scalar height_ = 530;
    static constexpr Scalar lengthX_ = 4500;
};

} // end namespace Dumux

#endif
