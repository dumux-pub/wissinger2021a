SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:  

Janis Wissinger
Modellierung von Deponien mit schwach radioaktivem Material
Bachelor Thesis, 2021
University of Stuttgart

Installation with Docker 
========================

Create a new folder in your favourite location and change into it

```bash
mkdir DUMUX-Radioactive
cd DUMUX-Radioactive
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/wissinger2021a/-/raw/master/docker/docker_wissinger2021a.sh
```
Open the Docker Container
````bash
bash docker_wissinger2021a.sh open
````

Installation
============

The easiest way to install this module and its dependencies is to create a new directory

```
mkdir DUMUX-Radioactive && cd DUMUX-Radioactive
```

and download the install script

[installwissinger2021a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/wissinger2021a/-/blob/master/installwissinger2021a.sh)

to that folder and run the script with

```
chmod u+x installwissinger2021a.sh
./installwissinger2021a.sh
```

Reproducing the results
=======================

After the script has run successfully, you may build the program executables

```
cd wissinger2021a/build-cmake/appl
make test_2pncmin_tpfa
```

and run them, e.g., with

```
./test_2pncmin_tpfa
```

